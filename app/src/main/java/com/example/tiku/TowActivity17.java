package com.example.tiku;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity17 extends AppCompatActivity {
    //编写一个程序，实现照片的浏览，向左滑动显示下一张图片，向右滑动显示上一张图片，并循环显示
    private ImageView img;

    private int[] imgs = {R.drawable.ic_launcher_background,R.drawable.ic_launcher_foreground,R.drawable.wa1,R.drawable.wa2};

    private int currentIndex = 0;
    private float mPosX;
    private float mPosY;
    private float mCurrentPosX;
    private float mCurrentPosY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow17);

        img = (ImageView) this.findViewById(R.id.two17_img);

        img.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (currentIndex > imgs.length-1) {
                    currentIndex = 0;
                }
                if (currentIndex<0) {
                    currentIndex = imgs.length-1;
                }
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mPosX = event.getX();
                        mPosY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        mCurrentPosX = event.getX();
                        mCurrentPosY = event.getY();
                        Log.i("MSG",currentIndex+"");
                        if (mCurrentPosX - mPosX > 100
                                && Math.abs(mCurrentPosY - mPosY) < 50){
                            img.setImageResource(imgs[currentIndex]);
                            currentIndex--;
                        }
                        else if (mPosX - mCurrentPosX > 100
                                && Math.abs(mCurrentPosY - mPosY) < 50){
                            img.setImageResource(imgs[currentIndex]);
                            currentIndex++;
                        }
                        Log.i("MSG",mPosX+"   "+mPosY);
                        Log.i("MSG",mCurrentPosX+"  "+mCurrentPosY);
                        Log.i("MSG",currentIndex+"");
                        break;
                }
                return true;
            }
        });
    }
}