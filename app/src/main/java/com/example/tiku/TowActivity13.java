package com.example.tiku;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity13 extends AppCompatActivity {
    EditText text;
    Button send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow13);

        text=findViewById(R.id.two13_et);
        send=findViewById(R.id.two13_btn);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TowActivity13.this,TowActivity13_2.class);
                Bundle bundle = new Bundle();
                bundle.putString("content",text.getText().toString());
                intent.putExtras(bundle);//将Bundle添加到Intent
                startActivityForResult(intent, 0);// 跳转并要求返回值，0代表请求值(可以随便写)
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Bundle bundle = data.getExtras();
            Toast.makeText(this, "回传的数据为:"+bundle.getString("content"), Toast.LENGTH_SHORT).show();
        }
    }
}