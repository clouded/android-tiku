package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity16 extends AppCompatActivity {
    TextView tvboy,tvgirl;
    EditText etfather,etmother;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one16);

        tvboy=(TextView)findViewById(R.id.one16_boy);
        tvgirl=(TextView)findViewById(R.id.one16_girl);
        etfather=(EditText)findViewById(R.id.one16_etfather);
        etmother=(EditText)findViewById(R.id.one16_etmother);
        btnok=(Button)findViewById(R.id.one16_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a=Float.parseFloat(etfather.getText().toString());
                float b=Float.parseFloat(etmother.getText().toString());
                tvboy.setText("男孩："+((a+b)*0.54)+"cm");
                tvgirl.setText("女孩："+(a*0.923+b)/2+"cm");
            }
        });
    }
}