package com.example.tiku;

import android.content.Intent;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity12_2 extends AppCompatActivity {
    TextView v1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow12_2);

        v1=(TextView)findViewById(R.id.two12_2_tv);

        Intent intent=this.getIntent();
        String content=intent.getStringExtra("content");

        v1.setText(content);
    }
}