package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity17 extends AppCompatActivity {
    EditText etstring,etchar;
    Button btnok;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one17);

        etchar=(EditText)findViewById(R.id.one17_etchar);
        etstring=(EditText)findViewById(R.id.one17_etstring);
        tv=(TextView)findViewById(R.id.one17_tv);
        btnok=(Button)findViewById(R.id.one17_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a=etstring.getText().toString();
                String b=etchar.getText().toString();
                int count=0;
                for(int i=0;i<a.length();i++){
                    if(a.charAt(i)==b.charAt(0)){
                        count++;
                    }
                }
                tv.setText(b+"出现的次数为:"+count);
            }
        });
    }
}