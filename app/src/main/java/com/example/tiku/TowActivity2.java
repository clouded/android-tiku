package com.example.tiku;

import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity2 extends AppCompatActivity {
    Button btn;
    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if (msg.arg1 == 1) {
                long time = System.currentTimeMillis()/1000;
                Toast.makeText(TowActivity2.this,time / 3600 % 24 +8 + ":" + time / 60 % 60 + ":" + time % 60,Toast.LENGTH_SHORT).show();
                //tv.setText(time / 3600 % 24 +8 + ":" + time / 60 % 60 + ":" + time % 60);
                //Toast.makeText(ThreadActivity.this,"success",Toast.LENGTH_LONG).show();
            }
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow2);
        btn=findViewById(R.id.two2_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyThread myThread=new MyThread();
                myThread.start();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(R.drawable.ic_launcher_background)
                        .setTitle("提示！")
                        .setMessage("确认退出程序？")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                break;
        }
        return false;
    }


    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            while (true) {
                Message msg=new Message();
                msg.arg1=1;
                handler.sendMessage(msg);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}