package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity12 extends AppCompatActivity {
    Button btnok;
    TextView tv;
    EditText et;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one12);

        tv=(TextView)findViewById(R.id.one12_tv);
        et=(EditText)findViewById(R.id.one12_et);
        btnok=(Button)findViewById(R.id.one12_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] a=et.getText().toString().split("-");
                tv.setText("数量输入框"+a[a.length-1]);
            }
        });
    }
}