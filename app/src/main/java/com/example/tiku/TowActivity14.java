package com.example.tiku;

import android.content.DialogInterface;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity14 extends AppCompatActivity {
    TextView et;
    Button btn;
    String[] str={"LYU","XX大学","XX学院"};
    private int picwhich = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow14);

        et = (TextView) findViewById(R.id.two14_et);
        btn = findViewById(R.id.two14_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //final boolean[] mulFlags = new boolean[] {true, false, false,false, false,false};
                AlertDialog.Builder dlg = new  AlertDialog.Builder(TowActivity14.this)
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star)
                .setTitle("选择学校")
                        .setSingleChoiceItems(str, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                picwhich = which;
                            }
                        })
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "学校：";
//                                for (int i = 0; i < mulFlags.length; i++)
//                                    if (mulFlags[i])  result += str[i] + "、";
                                result+=str[picwhich];
                                et.setText(result);
                            }
                        });

                dlg.create().show();
            }
        });
    }
}