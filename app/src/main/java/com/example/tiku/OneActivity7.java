package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity7 extends AppCompatActivity {
    TextView tv1,tv2;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one7);

        tv1=(TextView)findViewById(R.id.one7_first);
        tv2=(TextView)findViewById(R.id.one7_second);
        btnok=(Button) findViewById(R.id.one7_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=Integer.valueOf(tv1.getText().toString());
                int b=Integer.valueOf(tv2.getText().toString());
                Toast.makeText(OneActivity7.this,"Quotient is "+(a/b),Toast.LENGTH_LONG).show();
            }
        });
    }
}