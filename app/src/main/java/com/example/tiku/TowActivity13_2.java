package com.example.tiku;

import android.app.Activity;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity13_2 extends AppCompatActivity {
    TextView v1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow13_2);

        v1=findViewById(R.id.two13_2_tv);


        Intent intent=this.getIntent();
        String content=intent.getStringExtra("content");

        Bundle bundle = intent.getExtras();
        bundle.putString("content",content);
        intent.putExtras(bundle);
        this.setResult(Activity.RESULT_OK, intent);
        v1.setText(content);

        //this.finish();


    }
}