package com.example.tiku;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity9 extends AppCompatActivity {
    Button btnstart,btnstop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow9);

        btnstart=findViewById(R.id.two9_play);
        btnstop=findViewById(R.id.two9_stop);

        btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(TowActivity9.this,TowActivity9_service.class));
            }
        });
        btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {{
                stopService(new Intent(TowActivity9.this,TowActivity9_service.class));
            }
            }
        });
    }
}