package com.example.tiku;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity16 extends AppCompatActivity {
    String[] str={"xx学院","aa学院","hh学院"};
    Spinner sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow16);

        sp=(Spinner)findViewById(R.id.two16_spinner);

        BaseAdapter baseAdapter =new BaseAdapter() {
            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
//                LinearLayout liner = new LinearLayout(TowActivity16.this);
//                liner.setOrientation(LinearLayout.HORIZONTAL);

                TextView textView = new TextView(TowActivity16.this);
                textView.setText(str[position]);

//                liner.addView(textView);

                return textView;
            }
        };
        sp.setAdapter(baseAdapter);
    }
}