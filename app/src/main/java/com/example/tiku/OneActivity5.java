package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity5 extends AppCompatActivity {
    TextView tv1,tv2;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one5);

        tv1=(TextView)findViewById(R.id.one5_first);
        tv2=(TextView)findViewById(R.id.one5_second);
        btnok=(Button) findViewById(R.id.one5_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=Integer.valueOf(tv1.getText().toString());
                int b=Integer.valueOf(tv2.getText().toString());
                Toast.makeText(OneActivity5.this,"Difference is "+(a-b),Toast.LENGTH_LONG).show();
            }
        });
    }
}