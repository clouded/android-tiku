package com.example.tiku;

import android.os.Handler;
import android.os.Message;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class TowActivity5 extends AppCompatActivity {
    TextView tv;
    ImageView imgv;
    int[] img={R.drawable.ic_launcher_background,R.drawable.ic_launcher_foreground};
    String[] str={"介绍1","介绍2"};
    Random random=new Random();
    Handler handler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
                imgv.setImageResource(img[msg.arg1]);
                tv.setText(str[msg.arg1]);
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow5);
        imgv=findViewById(R.id.two5_img);
        tv=findViewById(R.id.two5_tv);

        MyThread myThread=new MyThread();
        myThread.start();
    }
    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            int i=0;
            while(true) {
                Message msg = new Message();
                if(i==0)
                    msg.arg1 = 0;
                else
                    msg.arg1 = 1;
                handler.sendMessage(msg);
                i+=1;i=i%2;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}