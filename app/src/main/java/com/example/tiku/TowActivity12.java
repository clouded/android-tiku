package com.example.tiku;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity12 extends AppCompatActivity {
    EditText text;
    Button send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow12);

        text=findViewById(R.id.two12_et);
        send=findViewById(R.id.two12_btn);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TowActivity12.this,TowActivity12_2.class);
                intent.putExtra("content",text.getText().toString());
                startActivity(intent);
            }
        });
    }
}