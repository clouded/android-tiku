package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity1 extends AppCompatActivity {
    TextView tv1,tv2;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one1);
        tv1=(TextView)findViewById(R.id.one_user);
        tv2=(TextView)findViewById(R.id.one_password);
        btnok=(Button) findViewById(R.id.one_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv1.getText().toString().equals("admin")&&tv2.getText().toString().equals("8888")){
                    Toast.makeText(OneActivity1.this,"登陆成功",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}