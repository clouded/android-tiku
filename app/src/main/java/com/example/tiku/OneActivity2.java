package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity2 extends AppCompatActivity {
    TextView tv1,tv2;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one2);

        tv1=(TextView)findViewById(R.id.one2_first);
        tv2=(TextView)findViewById(R.id.one2_second);
        btnok=(Button) findViewById(R.id.one2_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=Integer.valueOf(tv1.getText().toString());
                int b=Integer.valueOf(tv2.getText().toString());

                if(a>b){
                    Toast.makeText(OneActivity2.this,"Max is "+a,Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(OneActivity2.this,"Max is "+b,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}