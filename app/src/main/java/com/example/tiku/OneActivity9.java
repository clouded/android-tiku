package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity9 extends AppCompatActivity {
    Button btnok;
    TextView tv;
    EditText et;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one9);

        tv=(TextView)findViewById(R.id.one9_tv);
        et=(EditText)findViewById(R.id.one9_et);
        btnok=(Button)findViewById(R.id.one9_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] a=et.getText().toString().split("-");
                int sum=Integer.parseInt(a[0]);
                for(int i=1;i<a.length;i++){
                    sum-=Integer.parseInt(a[i]);
                }
                Toast.makeText(OneActivity9.this,"Result is "+sum,Toast.LENGTH_LONG).show();
            }
        });
    }
}