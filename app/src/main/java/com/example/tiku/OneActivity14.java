package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity14 extends AppCompatActivity {
    EditText et;
    TextView tv;
    Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one14);

        et=(EditText)findViewById(R.id.one14_et);
        tv=(TextView)findViewById(R.id.one14_tv);
        bt=(Button)findViewById(R.id.one14_btnok);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(et.getText().toString());
                long fact = 1;
                for (int i = 1; i <= a; i++) fact *= i;
                tv.setText("阶乘为:" + fact);
            }
        });
    }
}