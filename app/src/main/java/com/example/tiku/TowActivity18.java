package com.example.tiku;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class TowActivity18 extends AppCompatActivity {
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 0;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow18);
        lv=(ListView)findViewById(R.id.two18_ls);

        File[] files = Environment.getExternalStorageDirectory().listFiles();
        int count=0;
        for(int i=0;i<files.length;i++){
            if(getFileSuffix(files[i].getName()).equals(".txt")){
                count++;
            }
        }

        String[] str = new String[count+1];
        if(count==0){str[0]="这里没有txt后缀的文件";}
        else {
            for (int i = 0; i < count; i++) {
                str[i] = files[i].getName();
            }
        }
        //intent.getStringArrayExtra("fileList");
        //fileList();
        //System.out.println(files[0]+"555");
        ArrayAdapter adapter =new ArrayAdapter(
                TowActivity18.this,
                R.layout.two18_item,
                str
        );
        lv.setAdapter(adapter);
    }
    public static String getFileSuffix(String fileName){
        if(fileName.lastIndexOf(".")!=-1){
        return fileName.substring(fileName.lastIndexOf("."));
        }//例如：abc.png  截取后：.png
        else return "";
    }
//    private void getPermission(){
//        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}
//                    ,WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
//        }
//    }
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(requestCode==WRITE_EXTERNAL_STORAGE_REQUEST_CODE){
//            for (int i=0;i<permissions.length;i++){
//                Log.i("MainActivity", "申请的权限为：" + permissions[i] + ",申请结果：" + grantResults[i]);
//            }
//        }
//    }
}