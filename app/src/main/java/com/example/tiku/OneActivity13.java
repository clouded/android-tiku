package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class OneActivity13 extends AppCompatActivity {
    Button btnok;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one13);

        tv=(TextView)findViewById(R.id.one13_tv);
        btnok=(Button)findViewById(R.id.one13_btnok);
        final Random random =new Random();
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=random.nextInt(51)+50;
                int b=random.nextInt(51)+50;
                tv.setText("Result is "+(a-b));
            }
        });
    }
}