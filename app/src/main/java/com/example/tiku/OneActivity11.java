package com.example.tiku;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class OneActivity11 extends AppCompatActivity {
    TextView tv;
    Button bt;
    int i=0;
    int[] color={0xFF800000,0xFF000080,0xFF00FFFF,0xFFC0C0C0,0xFFA9A9A9,0xFF0000FF};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one11);

        tv=(TextView)findViewById(R.id.one11_tv);
        bt=(Button)findViewById(R.id.one11_btnok);
        final Random random=new Random();
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setTextColor(color[i]);
                i=(i+1)%6;
            }
        });
    }
}