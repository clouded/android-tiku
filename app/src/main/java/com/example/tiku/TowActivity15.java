package com.example.tiku;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity15 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow15);

        findViewById(R.id.two15_btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final ProgressDialog pro = new ProgressDialog(TowActivity15.this);
                pro.setTitle("进度条");
                pro.setMessage("请耐心等待");
                //设置最大值
                pro.setMax(100);
                //设置初始值
                pro.setProgress(0);
                //设置样式
                pro.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pro.setButton("后台处理",new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pro.dismiss();
                    }
                });
                //启动进度条
                pro.onStart();
                new Thread(){
                    public void run() {
                        for (int i = 0; i < 100; i++) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            //设置每次增加的度
                            pro.incrementProgressBy(1);

                        }
                        pro.dismiss();
                    };
                }.start();
                pro.show();

            }
        });
    }
}