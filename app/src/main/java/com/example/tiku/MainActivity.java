package com.example.tiku;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import androidx.annotation.ContentView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    Button one1,one2,one3,one4,one5,one6,one7,one8,one9,one10,one11,one12,one13,one14,one15,one16,one17,
            tow1,tow2,tow3,tow4,tow5,tow6,tow7,tow8,tow9,tow10,tow11,tow12,tow13,tow14,tow15,tow16,tow17,tow18,
            three1,three2,three3,three4,three5,three6,three7,three8,three9,three10,three11;
    Button[] btn={one1,one2,one3,one4,one5,one6,one7,one8,one9,one10,one11,one12,one13,one14,one15,one16,one17,
            tow1,tow2,tow3,tow4,tow5,tow6,tow7,tow8,tow9,tow10,tow11,tow12,tow13,tow14,tow15,tow16,tow17,tow18,
            three1,three2,three3,three4,three5,three6,three7,three8,three9,three10,three11};
    int[] str={R.id.One1,R.id.One2,R.id.One3,R.id.One4,R.id.One5,R.id.One6,R.id.One7,R.id.One8,R.id.One9,R.id.One10,
            R.id.One11,R.id.One12,R.id.One13,R.id.One14,R.id.One15,R.id.One16,R.id.One17,
            R.id.Two1,R.id.Two2,R.id.Two3,R.id.Two4,R.id.Two5,R.id.Two6,R.id.Two7,R.id.Two8,R.id.Two9,R.id.Two10,
            R.id.Two11,R.id.Two12,R.id.Two13,R.id.Two14,R.id.Two15,R.id.Two16,R.id.Two17,R.id.Two18,
            R.id.Three1,R.id.Three2,R.id.Three3,R.id.Three4,R.id.Three5,R.id.Three6,R.id.Three7,R.id.Three8,R.id.Three9,R.id.Three10,
            R.id.Three11};
    Class[] content={OneActivity1.class,OneActivity2.class,OneActivity3.class,OneActivity4.class,OneActivity5.class,
            OneActivity6.class,OneActivity7.class,OneActivity8.class,OneActivity9.class,OneActivity10.class,
            OneActivity11.class,OneActivity12.class,OneActivity13.class,OneActivity14.class,OneActivity15.class,
            OneActivity16.class,OneActivity17.class,
            TowActivity1.class,TowActivity2.class,TowActivity3.class,TowActivity4.class,TowActivity5.class,
            TowActivity6.class,TowActivity7.class,TowActivity8.class,TowActivity9.class,TowActivity10.class,
            TowActivity11.class,TowActivity12.class,TowActivity13.class,TowActivity14.class,TowActivity15.class,
            TowActivity16.class,TowActivity17.class,TowActivity18.class,
            UthreeActivity1.class,UthreeActivity2.class,UthreeActivity3.class,UthreeActivity4.class,UthreeActivity5.class,
            UthreeActivity6.class,UthreeActivity7.class,UthreeActivity8.class,UthreeActivity9.class,UthreeActivity10.class,
            UthreeActivity11.class};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for(int i=0;i<46;i++){
            btn[i]=findViewById(str[i]);
            final int finalI = i;
            btn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,content[finalI]);
                    startActivity(intent);
                }
            });
        }
    }
}