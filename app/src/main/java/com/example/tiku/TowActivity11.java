package com.example.tiku;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity11 extends AppCompatActivity {
    private ListView listView;
    //需要适配的数据
    private String[] names={"wang","li"};
    private String[] phone={"123456","45125623"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow11);
        //初始化ListView控件
        listView=(ListView)findViewById(R.id.two11_ls);
        //创建一个adapter的实例
        MyBaseAdapter adapter=new MyBaseAdapter();
        //设置adapter
        listView.setAdapter(adapter);
    }
    class MyBaseAdapter extends BaseAdapter{
        //得到Item的总数
        public int getCount(){
            //返回ListView Item条目的总数
            return names.length;
        }
        //得到Item代表的对象
        public Object getItem(int position){
            //返回ListView Item条目代表的对象
            return names[position];
        }
        //得到Item的id
        public long getItemId(int position){
            //返回ListView Item的id
            return position;
        }
        //得到Item的View视图
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //将list_item.xml文件找出来并转换成View对象
            View view=View.inflate(TowActivity11.this,R.layout.two11_item,null);
            //找到list_item.xml中创建的TextView
            TextView textView1=(TextView)view.findViewById(R.id.two11_item_tv1);
            textView1.setText(names[position]);
            TextView textView2=(TextView)view.findViewById(R.id.two11_item_tv2);
            textView2.setText(phone[position]);
            ImageView imageView=(ImageView)view.findViewById(R.id.two11_item_img);
            imageView.setBackgroundResource(R.drawable.ic_launcher_background);
            return view;
        }
    }

}