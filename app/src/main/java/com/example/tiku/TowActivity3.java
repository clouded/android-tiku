package com.example.tiku;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity3 extends AppCompatActivity {
    Button button;
    TextView textView;
    EditText editText;
    String str,temp = "";;
    Handler handler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if(msg.arg1==1){
                textView.setText(temp);
                temp="";
            }
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow3);

        button=findViewById(R.id.two3_btn);
        textView=findViewById(R.id.two3_tv);
        editText=findViewById(R.id.two3_et);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            MyThread myThread=new MyThread();
            myThread.start();
            }
        });
    }
    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            str=editText.getText().toString();
            for(int i=0;i<str.length();i++){
                for (int j=0;j<=i;j++){
                    temp+=str.charAt(j);
                }
                Message msg=new Message();
                msg.arg1=1;
                handler.sendMessage(msg);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}