package com.example.tiku;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.KeyEvent;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity1 extends AppCompatActivity {
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow1);

        tv=(TextView)findViewById(R.id.two_tv);

//        MyAsyncTask myAsyncTask = new MyAsyncTask();
//        myAsyncTask.execute(time);
        MyThread myThread=new MyThread();
        myThread.start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(R.drawable.ic_launcher_background)
                        .setTitle("提示！")
                        .setMessage("确认退出程序？")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                break;
        }
        return false;
    }


    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            while(true){
                long time = System.currentTimeMillis()/1000;
                tv.setText(time/ 3600 % 24 + 8 + ":" + time / 60 % 60 + ":" + time % 60);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }


//    class MyAsyncTask extends AsyncTask<Long,Long,String>{
//        @Override
//        protected String doInBackground(Long... longs) {
//            for(int i=0;;i++){
//                publishProgress(longs[0]);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        @Override
//        protected void onProgressUpdate(Long... values) {
//            super.onProgressUpdate(values);
//            tv.setText(values[0]/ 3600 % 24 + 8 + ":" + values[0] / 60 % 60 + ":" + values[0] % 60);
//        }
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            tv.setText("加载完毕！");
//        }
//    }
}