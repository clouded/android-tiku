package com.example.tiku;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class OneActivity4 extends AppCompatActivity {
    TextView tv1,tv2;
    Button btnok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one4);

        tv1=(TextView)findViewById(R.id.one4_first);
        tv2=(TextView)findViewById(R.id.one4_second);
        btnok=(Button) findViewById(R.id.one4_btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=Integer.valueOf(tv1.getText().toString());
                int b=Integer.valueOf(tv2.getText().toString());
                Toast.makeText(OneActivity4.this,"Sum is "+(a+b),Toast.LENGTH_LONG).show();
            }
        });
    }
}