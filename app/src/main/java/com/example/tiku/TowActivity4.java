package com.example.tiku;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class TowActivity4 extends AppCompatActivity {
    TextView tv;
    Button btn;
    Random random=new Random();
    Handler handler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if(msg.arg1==1){
                    int r=random.nextInt(21);
                    tv.setText("选中的数字是"+r);
                    Toast.makeText(TowActivity4.this,"选中的数字是"+r,Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow4);

        tv=findViewById(R.id.two4_tv);
        btn=findViewById(R.id.two4_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            MyThread myThread=new MyThread();
            myThread.start();
            }
        });
    }
    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            for(int i=0;i<6;i++) {
                Message msg = new Message();
                msg.arg1 = 1;
                handler.sendMessage(msg);

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}