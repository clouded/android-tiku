package com.example.tiku;

import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity8 extends AppCompatActivity {
    Button load;
    TextView tv;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow8);

        load=findViewById(R.id.two8_load);
        tv=findViewById(R.id.two8_tv);
        pb=findViewById(R.id.two8_pb);

        final MyAsyncTask myAsyncTask=new MyAsyncTask();
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MyAsyncTask myAsyncTask=new MyAsyncTask();

                myAsyncTask.execute("aaa");
                //myAsyncTask.cancel(true);
            }
        });
    }
    class MyAsyncTask extends AsyncTask<String,Integer,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tv.setText("准备加载");
        }

        @Override
        protected String doInBackground(String... strings) {
            //strings[0]
            for (int i=1;i<=100;i++){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }
            return "bbb";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pb.setProgress(values[0]);
            tv.setText("加载了"+values[0]+"%");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tv.setText("加载完毕！");
        }
    }
}