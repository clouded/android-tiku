package com.example.tiku;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class OneActivity10 extends AppCompatActivity {
    TextView tv;
    Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one10);

        tv=(TextView)findViewById(R.id.one10_tv);
        bt=(Button)findViewById(R.id.one10_btnok);
        final Random random=new Random();
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a=random.nextInt(255);
                int b=random.nextInt(255);
                int c=random.nextInt(255);
                tv.setTextColor(Color.rgb(a,b,c));
            }
        });
    }
}