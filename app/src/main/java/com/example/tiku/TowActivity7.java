package com.example.tiku;

import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class TowActivity7 extends AppCompatActivity {
//实现记录备忘录功能，将写好的备忘录保存到SD卡上，点击浏览的时候查看备忘录内容。
    EditText etcontent;
    Button btsave,btload;
    String content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow7);

        etcontent=findViewById(R.id.two7_content);
        btsave=findViewById(R.id.two7_save);
        btload=findViewById(R.id.two7_load);

        btsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content=etcontent.getText().toString();
                try {
                    String state= Environment.getExternalStorageState();
                    if(state.equals(Environment.MEDIA_MOUNTED)) {
                        File path=Environment.getExternalStorageDirectory();
                        //path.listFiles();
                        File file=new File(path,"forget.txt");
                        FileOutputStream fos = openFileOutput( "forget.txt", MODE_PRIVATE);
                        fos.write(content.getBytes());
                        fos.close();
                        System.out.println(path.getPath().toString());
                        Toast.makeText(TowActivity7.this, "保存成功!", Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        btload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content=etcontent.getText().toString();
                try {
                    FileInputStream fis=openFileInput("forget.txt");
                    byte[] buffer=new byte[fis.available()];
                    fis.read(buffer);
                    etcontent.setText(new String(buffer));
                    fis.close();
                    Toast.makeText(TowActivity7.this, "加载成功!", Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}