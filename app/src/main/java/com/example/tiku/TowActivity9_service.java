package com.example.tiku;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class TowActivity9_service extends Service {
    public TowActivity9_service() {
    }
    MediaPlayer player;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("MSG","onCreate");
        player = MediaPlayer.create(this, R.raw.vitality);
        player.setLooping(true);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("MSG","onStartCommand");
        player.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("MSG","onDestroy");
        player.stop();
        player.release();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        Log.i("MSG","onBind");
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
