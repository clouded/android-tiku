package com.example.tiku;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class TowActivity6 extends AppCompatActivity {
    CheckBox cbRemember;
    Button btnLogin;
    EditText etUserName,etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow6);

        cbRemember=(CheckBox) findViewById(R.id.two6_cb);
        btnLogin=(Button) findViewById(R.id.two6_btn);
        etUserName=(EditText) findViewById(R.id.two6_name);
        etPassword=(EditText) findViewById(R.id.two6_pwd);
        //界面显示时查找是否有保存的用户名和密码信息，如果有，显示到对应控件上

        final SharedPreferences preference = getSharedPreferences("login", MODE_PRIVATE);
        String name = preference.getString("name", "");
        String pwd = preference.getString("pwd", "");
        if (name.equals(""))
        {
            cbRemember.setChecked(false);
        }
        else {
            etUserName.setText(name);
            etPassword.setText(pwd);
            cbRemember.setChecked(true);
        }
        //登录按钮事件中判断是否选择复选按钮，保存用户名和密码信息
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String username=etUserName.getText().toString();
                String password=etPassword.getText().toString();
                //用户名和密码的验证
                if(username.equals("admin") && password.equals("123456")){
                    if (cbRemember.isChecked()){
                        SharedPreferences.Editor edit = preference.edit();
                        edit.putString("name", username);
                        edit.putString("pwd",password);

                        edit.commit();
                    }
                    Toast.makeText(TowActivity6.this,"登陆成功",Toast.LENGTH_SHORT).show();
                }else Toast.makeText(TowActivity6.this,"登陆失败",Toast.LENGTH_SHORT).show();

            }
        });
    }
}